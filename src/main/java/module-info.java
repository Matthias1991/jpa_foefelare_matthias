open module be.mijnbedrijf.jpalessons {
    requires java.persistence;
    requires org.hibernate.orm.core;
    requires java.sql;
    requires net.bytebuddy;
    requires java.xml.bind;
    requires java.desktop;
}