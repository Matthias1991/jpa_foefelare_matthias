package be.mijnbedrijf.jpalessons.messages;

import javax.persistence.*;


public class SaveMessage {
    public static void main(String[] args) {
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            Message message = new Message(1, "Epstein didn't kill himself");

            manager.persist(message);

            transaction.commit();

            System.out.println("Message saved");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }

    }
}
