package be.mijnbedrijf.jpalessons.messages;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Objects;

public class TestMessage {
    private static EntityManagerFactory factory;

    private static void print(int pos, Message memMessage){
        EntityManager manager = factory.createEntityManager();
        Message dbMessage = manager.find(Message.class, memMessage.getId());
        manager.close();
        System.out.println(pos + ": " + memMessage.getText() + "\t" + ((dbMessage != null) ? dbMessage.getText() : "null"));
    }

    public static void main(String[] args) {
        EntityManager manager = null;
        try{
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();

            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();
            Message inabottle = manager.find(Message.class, 1L);
            if (inabottle != null){
                manager.remove(inabottle);
            }
            transaction.commit();

            transaction.begin();
            inabottle = new Message(100, "AAAARGH");
            print(1, inabottle);

        }finally {

            if (Objects.nonNull(manager)) {
                manager.close();
            }
            if (Objects.nonNull(factory)) {
                factory.close();
            }
        }
    }
}
