package be.mijnbedrijf.jpalessons.persons;



import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class PeopleImpl implements PeopleRepo{

    public void savePeople(){
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            LocalDate geboorteDag = LocalDate.of(1991,10,25);
            Address waarLeefJeMan = new Address("Moorselestraat", "79", "8930", "Menen", "België");


            People dude = new People("Matthias", "Andriessen",
                    geboorteDag, People.Geslacht.IETS_ER_TUSSEN, false, waarLeefJeMan);

            manager.persist(dude);

            transaction.commit();

            System.out.println("Zou in DB moeten verschijnen");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
    }
}
