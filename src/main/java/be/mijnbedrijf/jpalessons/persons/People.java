package be.mijnbedrijf.jpalessons.persons;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;

@Entity
@Table(name="PEOPLE")
public class People implements Serializable {
    @Id
    @Column(name="ID_PEOPLE")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Version
    @Column(name="VERSION")
    private long versienummer;
    @Column(name="FIRST_NAME")
    @Basic(optional = false)
    private String voorNaam;
    @Column(name="LAST_NAME")
    @Basic(optional = false)
    private String achterNaam;
    @Column(name="BIRTHDAY")
    private LocalDate geboorteDag;
    @Column(name="GENDER")
    @Enumerated(EnumType.STRING)
    private Geslacht geslacht;
    @Column(name = "PICTURE")
    @Lob
    @Basic(fetch=FetchType.LAZY)
    private byte[] foto;
    @Column(name= "COMMENT")
    @Lob
    @Basic(fetch=FetchType.LAZY)
    private String commentaar;
    @Column(name= "MARRIED")
    private boolean getrouwd;
    @Column(name= "HOMEPAGE")
    private String thuispagina;

    @Embedded
    private Address address = new Address();

    public People() {
    }

    public People(String voorNaam, String achterNaam, LocalDate geboorteDag, Geslacht geslacht, boolean getrouwd, Address adres) {
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        this.geboorteDag = geboorteDag;
        this.geslacht = geslacht;
        this.getrouwd = getrouwd;
        this.address = adres;
    }



    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public enum Geslacht {
        MAN, VROUW, IETS_ER_TUSSEN, GEEN_VAN_BEIDE;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public long getVersienummer() {
        return versienummer;
    }

    private void setVersienummer(long versienummer) {
        this.versienummer = versienummer;
    }

    public String getVoorNaam() {
        return voorNaam;
    }

    public void setVoorNaam(String voorNaam) {
        this.voorNaam = voorNaam;
    }

    public String getAchterNaam() {
        return achterNaam;
    }

    public void setAchterNaam(String achterNaam) {
        this.achterNaam = achterNaam;
    }

    public LocalDate getGeboorteDag() {
        return geboorteDag;
    }

    public void setGeboorteDag(LocalDate geboorteDag) {
        this.geboorteDag = geboorteDag;
    }

    public Geslacht getGeslacht() {
        return geslacht;
    }

    public void setGeslacht(Geslacht geslacht) {
        this.geslacht = geslacht;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getCommentaar() {
        return commentaar;
    }

    public void setCommentaar(String commentaar) {
        this.commentaar = commentaar;
    }

    public boolean isGetrouwd() {
        return getrouwd;
    }

    public void setGetrouwd(boolean getrouwd) {
        this.getrouwd = getrouwd;
    }

    public String getThuispagina() {
        return thuispagina;
    }

    public void setThuispagina(String thuispagina) {
        this.thuispagina = thuispagina;
    }

    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", versienummer=" + versienummer +
                ", voorNaam='" + voorNaam + '\'' +
                ", achterNaam='" + achterNaam + '\'' +
                ", geboorteDag=" + geboorteDag +
                ", geslacht=" + geslacht +
                ", foto=" + Arrays.toString(foto) +
                ", commentaar='" + commentaar + '\'' +
                ", getrouwd=" + getrouwd +
                ", thuispagina='" + thuispagina + '\'' +
                '}';
    }
}
