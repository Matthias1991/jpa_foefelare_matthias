package be.mijnbedrijf.jpalessons.manytomany;

public interface MagazineRepo {
    void saveMagazine();
    void saveReader();
    Reader getReader(long id);
}
