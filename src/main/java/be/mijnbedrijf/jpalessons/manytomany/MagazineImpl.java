package be.mijnbedrijf.jpalessons.manytomany;

import be.mijnbedrijf.jpalessons.persons.Address;
import be.mijnbedrijf.jpalessons.persons.People;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class MagazineImpl implements MagazineRepo {
    @Override
    public void saveMagazine() {
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();



            transaction.commit();

            System.out.println("columns aangemaakt");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
    }

    @Override
    public void saveReader() {
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            Reader lezer = new Reader();
            Set magazines = new HashSet();
            magazines.add(new Magazine("knack"));
            magazines.add(new Magazine("humo"));
            lezer.setMagazines(magazines);

            manager.persist(lezer);

            transaction.commit();

            System.out.println("reader met magazines opgeslagen");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
    }

    @Override
    public Reader getReader(long id) {
        EntityManagerFactory factory = null;
        EntityManager manager = null;
        Reader foundReader;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            foundReader = manager.find(Reader.class, id);

            transaction.commit();

            foundReader.getMagazines().size();

            return foundReader;


        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }

    }
}
