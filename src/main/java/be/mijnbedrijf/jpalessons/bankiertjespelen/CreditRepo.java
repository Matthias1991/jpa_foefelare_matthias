package be.mijnbedrijf.jpalessons.bankiertjespelen;

public interface CreditRepo {
    void changeCredit(Credit credit, long id);
    void createCredit(Credit credit);

}
