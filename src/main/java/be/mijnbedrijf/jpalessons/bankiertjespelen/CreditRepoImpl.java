package be.mijnbedrijf.jpalessons.bankiertjespelen;

import be.mijnbedrijf.jpalessons.visitors.Visitor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Objects;

public class CreditRepoImpl implements CreditRepo {
    @Override
    public void changeCredit(Credit credit, long id) {
        Credit creditinDB = null;
        EntityManagerFactory managerFactory = null;
        EntityManager manager = null;

        try {
            managerFactory = Persistence.createEntityManagerFactory("dinges");
            manager = managerFactory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            creditinDB = manager.find(Credit.class, id);
            

            transaction.commit();

        } finally {
            if (Objects.nonNull(manager)) {
                manager.close();
            }
            if (Objects.nonNull(managerFactory)) {
                managerFactory.close();
            }
        }
    }

    @Override
    public void createCredit(Credit credit) {
        EntityManagerFactory managerFactory = null;
        EntityManager manager = null;

        try {
            managerFactory = Persistence.createEntityManagerFactory("dinges");
            manager = managerFactory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            manager.persist(credit);

            transaction.commit();


        } finally {
            if (Objects.nonNull(manager)) {
                manager.close();
            }
            if (Objects.nonNull(managerFactory)) {
                managerFactory.close();
            }
        }
    }
}
