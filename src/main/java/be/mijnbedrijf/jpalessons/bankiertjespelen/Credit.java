package be.mijnbedrijf.jpalessons.bankiertjespelen;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import java.util.Objects;

@Entity
public class Credit {
    @Id
    private long id;
    @Version
    private int version;

    private long stackz;

    public Credit() {
    }

    public Credit(long stackz) {
        this.stackz = stackz;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getStackz() {
        return stackz;
    }

    public void setStackz(long stackz) {
        this.stackz = stackz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return getId() == credit.getId() &&
                getVersion() == credit.getVersion() &&
                getStackz() == credit.getStackz();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getVersion(), getStackz());
    }
}
