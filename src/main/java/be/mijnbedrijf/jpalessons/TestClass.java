package be.mijnbedrijf.jpalessons;

import be.mijnbedrijf.jpalessons.bankiertjespelen.Credit;
import be.mijnbedrijf.jpalessons.bankiertjespelen.CreditRepo;
import be.mijnbedrijf.jpalessons.bankiertjespelen.CreditRepoImpl;
import be.mijnbedrijf.jpalessons.manytomany.Magazine;
import be.mijnbedrijf.jpalessons.manytomany.MagazineImpl;
import be.mijnbedrijf.jpalessons.manytomany.MagazineRepo;
import be.mijnbedrijf.jpalessons.manytomany.Reader;
import be.mijnbedrijf.jpalessons.persons.PeopleImpl;
import be.mijnbedrijf.jpalessons.persons.PeopleRepo;
import be.mijnbedrijf.jpalessons.school.School;
import be.mijnbedrijf.jpalessons.school.SchoolRepo;
import be.mijnbedrijf.jpalessons.school.SchoolRepoImpl;
import be.mijnbedrijf.jpalessons.school.Student;
import be.mijnbedrijf.jpalessons.sick.Patient;
import be.mijnbedrijf.jpalessons.sick.PatientRepo;
import be.mijnbedrijf.jpalessons.sick.PatientRepoImpl;
import be.mijnbedrijf.jpalessons.visitors.Visitor;
import be.mijnbedrijf.jpalessons.visitors.VisitorRepo;
import be.mijnbedrijf.jpalessons.visitors.VisitorRepoImpl;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TestClass {
    public static void main(String[] args) {

        MagazineRepo repo = new MagazineImpl();

       Reader r = repo.getReader(1);
       Set<Magazine> s = r.getMagazines();

        System.out.println("test");
        System.out.println(r.getId());

        for (Magazine el: s){
            System.out.println(el.getTitle());
        }





    }
}
