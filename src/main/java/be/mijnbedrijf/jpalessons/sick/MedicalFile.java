package be.mijnbedrijf.jpalessons.sick;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class MedicalFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private float height;
    private float weight;

    @OneToOne
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "MedicalFile{" +
                "id=" + id +
                ", height=" + height +
                ", weight=" + weight +
                ", patient=" + patient +
                '}';
    }
}
