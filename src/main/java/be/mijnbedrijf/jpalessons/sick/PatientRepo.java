package be.mijnbedrijf.jpalessons.sick;

public interface PatientRepo {
    void savePatient();
    Patient getPatient(long ID);
}
