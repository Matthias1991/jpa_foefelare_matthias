package be.mijnbedrijf.jpalessons.sick;

import be.mijnbedrijf.jpalessons.persons.Address;
import be.mijnbedrijf.jpalessons.persons.People;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class PatientRepoImpl implements PatientRepo{
    @Override
    public void savePatient() {
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            Patient coronaIsEenHoax = new Patient("Matthias");

            manager.persist(coronaIsEenHoax);

            transaction.commit();

            System.out.println("Patient saved");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
    }

    @Override
    public Patient getPatient(long ID) {
        Patient patient = null;
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            patient = manager.find(Patient.class, ID);

            transaction.commit();

        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }

        return patient;

    }
}
