package be.mijnbedrijf.jpalessons.visitors;

public interface VisitorRepo {
    void save(Visitor visitor);
    Visitor findVisitor(long id);
    void changeVisitor(long id, String newname);
}
