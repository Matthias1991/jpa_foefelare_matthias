package be.mijnbedrijf.jpalessons.visitors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Objects;

public class VisitorRepoImpl implements VisitorRepo {
    @Override
    public void save(Visitor visitor) {
        EntityManagerFactory managerFactory = null;
        EntityManager manager = null;

        try {
            managerFactory = Persistence.createEntityManagerFactory("dinges");
            manager = managerFactory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            manager.persist(visitor);

            transaction.commit();

            System.out.println("Visitor saved: " + visitor);
        } finally {
            if (Objects.nonNull(manager)) {
                manager.close();
            }
            if (Objects.nonNull(managerFactory)) {
                managerFactory.close();
            }
        }
    }

    @Override
    public Visitor findVisitor(long id) {
        Visitor visitor = null;
        EntityManagerFactory managerFactory = null;
        EntityManager manager = null;

        try {
            managerFactory = Persistence.createEntityManagerFactory("dinges");
            manager = managerFactory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            visitor = manager.find(Visitor.class, id);
            System.out.println("We're working with: " + visitor + " from DB!");

            transaction.commit();

        } finally {
            if (Objects.nonNull(manager)) {
                manager.close();
            }
            if (Objects.nonNull(managerFactory)) {
                managerFactory.close();
            }
        }


        return visitor;
    }

    @Override
    public void changeVisitor(long id, String newname) {
        Visitor visitor = null;
        EntityManagerFactory managerFactory = null;
        EntityManager manager = null;
        try{
            managerFactory = Persistence.createEntityManagerFactory("dinges");
            manager = managerFactory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            visitor = manager.find(Visitor.class, id);

            System.out.println("We're working with: " + visitor);

            visitor.setName(newname);
            System.out.println("We've changed that name for ya");

            transaction.commit();
            manager.close();

        }finally {
            if (managerFactory != null){
                managerFactory.close();
            }
        }
    }
}
