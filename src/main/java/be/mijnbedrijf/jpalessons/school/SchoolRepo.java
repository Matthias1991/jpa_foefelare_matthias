package be.mijnbedrijf.jpalessons.school;

public interface SchoolRepo {
    void saveSchool(School school);
    School getMeAschool(long id);
}
