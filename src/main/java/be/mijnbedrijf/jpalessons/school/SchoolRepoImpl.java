package be.mijnbedrijf.jpalessons.school;

import be.mijnbedrijf.jpalessons.persons.Address;
import be.mijnbedrijf.jpalessons.persons.People;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class SchoolRepoImpl implements SchoolRepo{
    @Override
    public void saveSchool(School school) {
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            manager.persist(school);

            transaction.commit();

            System.out.println("School aangemaakt");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
    }

    @Override
    public School getMeAschool(long id) {
        School nogeenschool = null;
        EntityManagerFactory factory = null;
        EntityManager manager = null;

        try {
            factory = Persistence.createEntityManagerFactory("dinges");
            manager = factory.createEntityManager();
            EntityTransaction transaction = manager.getTransaction();

            transaction.begin();

            nogeenschool = manager.find(School.class, id);

            transaction.commit();

            System.out.println("School aangemaakt");
        } finally {
            if (manager != null){
                manager.close();
            }
            if (factory != null){
                factory.close();
            }
        }
        return nogeenschool;
    }
}
